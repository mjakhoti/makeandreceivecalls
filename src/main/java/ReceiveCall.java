
import static spark.Spark.post;

import com.twilio.twiml.VoiceResponse;
import com.twilio.twiml.voice.Say;

public class ReceiveCall {

	public static void main(String[] args) {
		
		post("receive-call", (req, res) -> {
			
			VoiceResponse twiml = new VoiceResponse.Builder()
					.say(new Say.Builder("Hey MJ, this is the sample call app using Twilio. So easy & so much fun!")
							.build())
					.build();
			
			return twiml.toXml();
		});
		
	}

}
