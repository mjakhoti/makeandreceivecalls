
import java.net.URI;
import java.net.URISyntaxException;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.type.PhoneNumber;

public class MakeCall {
	
	public static final String ACCOUNT_SID = "<ACCOUNT SID>";
    public static final String AUTH_TOKEN = "<AUTH_TOKEN>";

    public static void main(String[] args) throws URISyntaxException {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        String from = "<TWILIO NUMBER>";
        String to = "<YOUR NUMBER>";		// verified number to make call to

        Call call = Call.creator(
        		new PhoneNumber(to), 
        		new PhoneNumber(from),
        		new URI("http://demo.twilio.com/docs/voice.xml")
    		).create();

        System.out.println(call.getSid());
    }

}
